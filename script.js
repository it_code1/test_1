let razmer=5; //устанавливаем размер поля
function start(){
	let div = document.createElement('div');
	div.className = "tableGame";
	div.innerHTML = "<table><tr><td>Ooops</td></tr></table>";
	div.innerHTML += "<table><tr><td>Yeah</td></tr></table>";
	panel_1_1.replaceWith(div);
}
function uploadNewWords(input) {
	let file = input.files[0];
	let reader = new FileReader();
	reader.readAsText(file);
	reader.onload = function() {
		let AllWords = new Array();
		AllWords = reader.result.split(' '); //Получили массив вообще всех слов с документа
		let WordsGame = new Array(); //Массив слов выбранных рандомно для игры
		let CapitanWords = new Array(); //в этом массиве будут отмечены ячейки, закрепленные за капитаном (командой)
		let random; //прост переменная
		for (let i=0; i<(razmer**2);i++){  //Получаем массив для конкретной игры
			while (WordsGame[i]==0 || WordsGame[i]==undefined){
				random = Math.floor(Math.random() * 1000);
				WordsGame[i]=AllWords[random];
				AllWords[random]=0;
			}
		}
		for (let i=0; i<9;i++){ //выбираем какие слова будут закреплены за капитаном
			random = Math.floor(Math.random() * 25);
			while(CapitanWords[random]==1){
				random = Math.floor(Math.random() * 25);
			}
			CapitanWords[random]=1;
		}
		for (let i=0; i<8;i++){ //выбираем какие слова будут закреплены за противником (игрой)
			random = Math.floor(Math.random() * 25);
			while(CapitanWords[random]==1 || CapitanWords[random]==2){
				random = Math.floor(Math.random() * 25);
			}
			CapitanWords[random]=2;
		}
		random = Math.floor(Math.random() * 25); //выбираем какое слово будет закреплено за убийцей
		while(CapitanWords[random]==1 || CapitanWords[random]==2){
			random = Math.floor(Math.random() * 25);
		}
		CapitanWords[random]=666;
        
        		document.write("<table align='center' cellspacing='5px' border-radius='30'>");
		for (let i=0; i<razmer;i++){
			document.write("<tr>");
			for (let j=0; j<razmer;j++){
				switch (CapitanWords[i*razmer+j]){
					case 1:
						document.write("<td bgcolor='mediumBlue' width='115' height='80' align='center'>"+(i*razmer+j+1)+'. '+WordsGame[i*razmer+j]+"</td>");
						break;
					case 2:
						document.write("<td bgcolor='red' width='115' height='80' align='center'>"+(i*razmer+j+1)+'. '+WordsGame[i*razmer+j]+"</td>");
						break;
					case 666:
						document.write("<td bgcolor='DarkGray' width='115' height='80' align='center'>"+(i*razmer+j+1)+'. '+WordsGame[i*razmer+j]+"</td>");
						break;
					default:
						document.write("<td bgcolor='#FFFFE0' width='115' height='80' align='center'>"+(i*razmer+j+1)+'. '+WordsGame[i*razmer+j]+"</td>");
						break;
				}
			}
			document.write("</tr>");
		}
		document.write("</table>");
		document.write("<p align='center'><input type='submit' name='clear' value='Скрыть выделенные слова'></input></p><br>");
		function clear() {
			alert('hi');
		};
		var clearButton = document.search.clear;
		clearButton.addEventListener("click", clear);
	};
	reader.onerror = function() {
		console.log(reader.error);
	};
};